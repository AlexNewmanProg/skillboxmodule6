﻿namespace Assets.Scripts.Models
{
    /// <summary>
    /// Сложность
    /// </summary>
    public enum Difficulty
    {
        easy,
        normal,
        hard
    }
}
