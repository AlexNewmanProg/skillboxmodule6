﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    /// <summary>
    /// Настройки уровня
    /// </summary>
    public class LevelSettings
    {
        /// <summary>
        /// Код от замка
        /// </summary>
        public int[] LockCode;

        /// <summary>
        /// Начальное значение
        /// </summary>
        public int[] StartCode;
    }
}
