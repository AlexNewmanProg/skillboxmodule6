﻿using UnityEngine;

namespace Assets.Scripts
{
    public partial class GameLogic
    {
        /// <summary>
        /// Обновить таймер
        /// </summary>
        private void UpdateTimer()
        {
            var time = Mathf.Round(Time.time);
            
            if (IsGameStarted)
            {
                _displayTime = Mathf.RoundToInt(TimeLimit - (time - TimeDiff));

                if (_displayTime <= 0)
                {
                    _displayTime = 0;
                }

                DisplayTablo(_displayTime);
            }
            else
            {
                DisplayTablo(TimeLimit);
            }
        }

        /// <summary>
        /// Отображает оставшееся время
        /// </summary>
        /// <param name="time">Время которое отобразить</param>
        private void DisplayTablo(int time)
        {
            TimerTablo.text = $"Время: {time}";
        }

        /// <summary>
        /// Запускает таймер
        /// </summary>
        private void StartTimer()
        {
            TimeDiff = (int) Mathf.Round(Time.time);
            IsGameStarted = true;
        }

        /// <summary>
        /// Останавливает таймер
        /// </summary>
        private void StopTimer()
        {
            IsGameStarted = false;
            _displayTime = TimeLimit;
        }
    }
}
