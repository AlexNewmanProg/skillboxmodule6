﻿using Assets.Scripts.Models;
using System;

namespace Assets.Scripts
{
    public partial class GameLogic
    {
        /// <summary>
        /// Лимит времени
        /// </summary>
        public int TimeLimit;

        /// <summary>
        /// Текущее значение времени
        /// </summary>
        private int _displayTime;

        /// <summary>
        /// Разность во времени между запуском и началом игры
        /// </summary>
        private int TimeDiff;

        /// <summary>
        /// Начата ли игра?
        /// </summary>
        private bool IsGameStarted = false;        

        /// <summary>
        /// Требуемое значение кода замка
        /// </summary>
        private int[] LockCode = new int[3];

        /// <summary>
        /// Текущее значение кода замка
        /// </summary>
        private int[] CurrentLockCode = new int[3];

        /// <summary>
        /// Стартовое значение кода замка
        /// </summary>
        private int[] StartLockCode = new int[3];

        /// <summary>
        /// Сложность игры
        /// </summary>
        private Difficulty _difficulty;
    }
}
