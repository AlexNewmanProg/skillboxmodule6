﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public partial class GameLogic : MonoBehaviour
    {    
        // Update is called once per frame
        void Update()
        {
            UpdateTimer();

            if(_displayTime == 0
                && IsGameStarted)
            {
                GameOver();
            }
        }   

        /// <summary>
        /// Начинает игру
        /// </summary>
        public async void StartGame()
        {
            await ConfigureLevelStateAsync();
            StartTimer();
        }  
        
        /// <summary>
        /// Перезапускает уровень
        /// </summary>
        public void RestartGame()
        {
            StopTimer();

            CurrentLockCode = StartLockCode;
            UpdateLockCode();
            ConfigureGameState(_difficulty);
            StartGame();

            _displayTime = TimeLimit;
        }
        
        /// <summary>
        /// Останавливает игру
        /// </summary>
        public void StopGame()
        {
            StopTimer();
        }

        /// <summary>
        /// Проигрыш
        /// </summary>
        private void GameOver()
        {
            StopTimer();
            TimeLimit = 0;
            GameoverMenu.SetActive(true);
        }

        /// <summary>
        /// Победа!
        /// </summary>
        private void Victory()
        {
            StopTimer();
            WinningMenu.SetActive(true);
        }

        /// <summary>
        /// Выход в главное меню
        /// </summary>
        public void MainMenu()
        {
            StopGame();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }
    }
}