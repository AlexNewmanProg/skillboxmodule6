﻿using Assets.Scripts.Models;
using System.Collections.Generic;

namespace Assets.Scripts
{
    /// <summary>
    /// Хранилище настроек уровней
    /// </summary>
    public static class LevelsSettingsStorage
    {
        /// <summary>
        /// Возвращает уровень по сложности
        /// </summary>
        /// <param name="difficulty">Сложность</param>
        /// <returns>Уровень</returns>
        public static LevelSettings GetLevelByDifficulty(Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.easy:
                    return new LevelSettings
                    {
                        LockCode = new int[3] { 5, 5, 5 },
                        StartCode = new int[3] { 0, 5, 0 }
                    };
                case Difficulty.normal:
                    return new LevelSettings
                    {
                        LockCode = new int[3] { 3, 4, 7 },
                        StartCode = new int[3] { 2, 7, 9 }
                    };
                case Difficulty.hard:
                    return new LevelSettings
                    {
                        LockCode = new int[3] { 1, 1, 1 },
                        StartCode = new int[3] { 0, 4, 3 }
                    };
                default:
                    return new LevelSettings
                    {
                        LockCode = new int[3] { 5, 5, 5 },
                        StartCode = new int[3] { 0, 5, 0 }
                    };
            }
        }
    }
}
