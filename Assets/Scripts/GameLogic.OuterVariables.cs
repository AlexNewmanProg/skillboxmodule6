﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public partial class GameLogic
    {
        /*
         * Ячейки для хранения замка для отображения
         */
        /// <summary>
        /// Первая ячейка кода замка
        /// </summary>
        public Text FirstLockCell;

        /// <summary>
        /// Вторая ячейка кода замка
        /// </summary>
        public Text SecondLockCell;

        /// <summary>
        /// Третья ячейка кода замка
        /// </summary>
        public Text ThirdLockCell;

        /// <summary>
        /// Таймер
        /// </summary>
        public Text TimerTablo;

        /// <summary>
        /// Победное меню
        /// </summary>
        public GameObject WinningMenu;

        /// <summary>
        /// Проигрышное меню
        /// </summary>
        public GameObject GameoverMenu;

    }
}
