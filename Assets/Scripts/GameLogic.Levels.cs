﻿using Assets.Scripts.Models;
using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public partial class GameLogic : MonoBehaviour
    {
        /// <summary>
        /// Устанавливает начальные значения уровня
        /// </summary>
        private async Task ConfigureLevelStateAsync()
        {
            var levelSettings = LevelsSettingsStorage.GetLevelByDifficulty(_difficulty);

            LockCode = levelSettings.LockCode;
            StartLockCode = levelSettings.StartCode;
            CurrentLockCode = levelSettings.StartCode;

            await DisplayLockCodeAsync();
        }

        /// <summary>
        /// Отображает код замка
        /// </summary>
        /// <returns></returns>
        private async Task DisplayLockCodeAsync()
        {
            /*
             * Для начала дадим пользователю посмотреть
             * какой код должен быть, а потом меняем его
             */
            FirstLockCell.text = LockCode[0].ToString();
            SecondLockCell.text = LockCode[1].ToString();
            ThirdLockCell.text = LockCode[2].ToString();

            /*Ждем немного*/
            await Task.Delay(TimeSpan.FromSeconds(1));

            UpdateLockCode();
        }

        /// <summary>
        /// Задает параметры игровой логики
        /// </summary>
        /// <param name="difficulty">Сложность</param>
        public void ConfigureGameState(string difficulty)
        {
            if (Enum.TryParse<Difficulty>(difficulty, out var result))
            {
                _difficulty = result;
            }
            else
            {
                _difficulty = Difficulty.easy;
            }

            ConfigureGameState(_difficulty);            
        }

        /// <summary>
        /// Задает параметры игровой логики
        /// </summary>
        /// <param name="difficulty">Сложность</param>
        private void ConfigureGameState(Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.easy:
                    TimeLimit = 80;
                    break;
                case Difficulty.normal:
                    TimeLimit = 60;
                    break;
                case Difficulty.hard:
                    TimeLimit = 45;
                    break;
                default:
                    break;
            }
        }
    }
}
