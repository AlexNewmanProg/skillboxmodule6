﻿using System;

namespace Assets.Scripts
{
    public partial class GameLogic
    {
        /// <summary>
        /// Значения на которые изменяет отвертка
        /// </summary>
        private int[] screwdriverValues = new int[3] { 2, -3, 1 };

        /// <summary>
        /// Значения на которые изменяет ломик
        /// </summary>
        private int[] crowbarValues = new int[3] { -1, 3, 2 };

        /// <summary>
        /// Значения на которые изменяет заколка
        /// </summary>
        private int[] barretteValues = new int[3] { 4, 0, 2 };

        /// <summary>
        /// Использование отвертки
        /// </summary>
        public void ApplyScrewdriverFacility()
        {
            ChangeLockCode(screwdriverValues[0], screwdriverValues[1], screwdriverValues[2]);
        }

        /// <summary>
        /// Использование ломика
        /// </summary>
        public void ApplyCrowbarFacility()
        {
            ChangeLockCode(crowbarValues[0], crowbarValues[1], crowbarValues[2]);
        }

        /// <summary>
        /// Использование заколки
        /// </summary>
        public void ApplyBarretteFacility()
        {
            ChangeLockCode(barretteValues[0], barretteValues[1], barretteValues[2]);
        }

        /// <summary>
        /// Обновляет значения кода замка
        /// </summary>
        private void UpdateLockCode()
        {
            FirstLockCell.text = CurrentLockCode[0].ToString();
            SecondLockCell.text = CurrentLockCode[1].ToString();
            ThirdLockCell.text = CurrentLockCode[2].ToString();
        }

        /// <summary>
        /// Изменяет значение кодов замка
        /// </summary>
        /// <param name="first">Первое изменяемое значение</param>
        /// <param name="second">Второе изменяемое значение</param>
        /// <param name="third">Третье изменяемое значение</param>
        private void ChangeLockCode(int first, int second, int third)
        {
            CurrentLockCode[0] = CountLockCode(CurrentLockCode[0], first);
            CurrentLockCode[1] = CountLockCode(CurrentLockCode[1], second);
            CurrentLockCode[2] = CountLockCode(CurrentLockCode[2], third);

            UpdateLockCode();

            if (IsWinningCode())
            {
                Victory();
            }
        }

        /// <summary>
        /// Считает новый код
        /// </summary>
        /// <param name="currentCode">Текущее значение</param>
        /// <param name="value">Добавляемое значение</param>
        /// <returns>Новое значение кода</returns>
        private int CountLockCode(int currentCode, int value)
        {
            int result = currentCode + value;

            if(result > 9)
            {
                var cucle = result - 9;
                result = -1 + cucle;
            }
            else if(result < 0)
            {
                result = 10 - Math.Abs(result);
            }

            return result;
        }

        /// <summary>
        /// Проверяет на выигрышную комбинацию
        /// </summary>
        /// <returns>
        /// true если совпадает
        /// иначе false
        /// </returns>
        private bool IsWinningCode()
        {
            return (CurrentLockCode[0] == LockCode[0]
                    && CurrentLockCode[1] == LockCode[1]
                        && CurrentLockCode[2] == LockCode[2]); 
        }
    }
}
